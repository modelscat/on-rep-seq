# README #

## ON-rep-seq data analysis pipeline ###

#### This Repo contains bioinformatic pipeline for strain identification by the ON-rep-seq method (Version 1.0)
#### ON-rep-seq pipeline is divided in four separated scripts or "wrappers" in order to ease debugging. It has been tested on ubuntu 16.04 LTS and OSX 10.13 (High Sierra). It allows for end-to-end data analysis of ON-rep-seq data starting from raw fast5 format files.


### Installation of dependencies ###

* Conda
	* https://conda.io/docs/user-guide/install/macos.html
####
		conda install -c bioconda cutadapt
#  
* Guppy Local basecalling for MinKNOW
	* https://community.nanoporetech.com/downloads
#
* Porechop (modified)
    * https://github.com/macieksk/Porechop branches `porechop_v0.2.2_fp_mod (tested)`, `porechop_v0.2.3_fp_mod (tested)`, `porechop_v0.2.4_fp_mod (untested)`, a submodule to this repo (originally forked from https://github.com/rrwick/Porechop )
	* NOTE! This Porechop modification requires replacemen of file adapter.py in the Porechop/porechop directory with the modified adapter.py file from this repository (see Downloads). This will allow you to do the analysis of all 192 adapters.
	* Secondly this Porechop modification has new options `--trimgtgrange`, `--fp2ndrun` which allow to trim multiple GTG repeats from ON-rep-seq reads. `scripts/porechop_fp2ndrun.sh` exemplifies how these can be used.
#
* Java SE Development kit 8
	* http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
#
* Homebrew (OSX only)
###
		ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null 2> /dev/null
#
* Parallel (OSX only)
###
		brew install parallel
#
* Canu version 1.7.1
	* https://github.com/marbl/canu/releases 
#
* Usearch v10 (free version 32-bits)
	* https://www.drive5.com/usearch/manual/install.html
#
* Classifiers
	* https://ccb.jhu.edu/software/kraken2/ 
	 (we recommend kraken2 for classification of corrected reads)
	 
	* https://github.com/infphilo/centrifuge
	(for less powerful machines Centrifuge or MiniKraken can also be used)
####
		git clone https://github.com/infphilo/centrifuge
		cd centrifuge
		make
		sudo make install prefix=/usr/local



### Usage

1)	Make sure you have successfully installed all dependences
##
2)	Place all scripts in the directory together with the “reads” folder generated during sequencing.
##
		Make sure all scrips are executable: 
		chmod +x *.sh
##
3)	Run 0.basecall_and_demultiplex.sh script.
####
		./0.basecall_and_demultiplex.sh
####
		If your data were live basecalled during the run answer “n” to the prompt question and drag the folder with basecalled files to terminal and press enter.
##
4)	Run 1.fastq2peaks.sh 
####
		./1.fastq2peaks.sh
####
		The script generates several folders with the following outputs:
		a) flowdiagrams/: a summary of high-quality reads and their size generated per sample
		b) peaks/: average peak profiles determined for each sample based on read distribution and abundance
		c) folder_per_sample/: raw reads in fastq, as well as a summary of 	peak detection and coordinates for every sample
		d) failed_samples/: samples (in fastq) that failed to generate enough reads after demultiplexing
##
5) Run 2.peak_correction.sh
####
		./2.peak_correction.sh
####
        The script generates several folders with the following outputs:
		a) folder_per_sample/input_peaks/: fastq files containing the sequences selected according to the summary of peak detection and coordinates for every sample.
		b) folder_per_sample/fixed_peaks/: fasta file containing sequences that have been subjected to error correction by CANU.
		c) for-centrifuge-corrected-peaks/: takes corrected reads from 	“../folder_per_sample/fixed_peaks/” and generate a single fasta file containing a consensus sequence for every peak and for every sample.
##
6) Run 3.taxonomy_assignment.sh [taxonomy is assigned with kraken2 or centrifuge] 
        (note that kraken2 is recomended especially whenever novel bacteria are expected)
####
		a) taxonomy_assignment/: Contains taxonomy annotation of the consensus 	sequences located at ‘for_classification_corrected_LCp/’. 
####
NOTE! 
		
		0.basecall_and_demultiplex.sh:	needs to be adjusted for Linux or OSX, see cores detection in lines 5 & 6
		2.peak_correction.sh: 			needs to be adjusted for Linux or OSX, see cores detection in lines 10 & 11. Similar to the path of CANU in lines 40-41
		3.taxonomy_assignment.sh: 		needs to be adjusted for Linux or OSX, see cores detection in lines 5 & 6

##
7) Run LCpCluster.R on LCp profiles located at 'flowgrams/'
       
####
		$ LCpCluster.R -h

        Usage: LCpCluster.R [-h|--help] <rep_sample_dir> [output_file ending with .ipynb or .html]

        rep_sample_dir  directory containing rep*.txt files with read lengths counts
        output_file     name of the jupyter execution result (default: runnable_jupyter_on-rep-seq_flowgrams_clustering_heatmaps.execution_result.ipynb ) 

An example executed notebook on the data from the publication can be viewed here: https://nbviewer.jupyter.org/gist/macieksk/736d0f988c11b88d4a5b88bc468c6261

Put a symbolic link to `LCpCluster.R` in `~/.local/bin/` or another folder in your `$PATH`. Run either one of these:

`$ LCpCluster.R flowgrams/ lengths_counts_profiles_clustering.ipynb`

`$ LCpCluster.R flowgrams/ lengths_counts_profiles_clustering.html`

A jupyter notebook (or html version) with clustering on LCprofiles will be produced. `.Rdata` image will be saved in `r_saved_images` subdir inside current dir. The code to load this image and continue working inside jupyter is provided in the second input cell (requires uncommenting a line).
####



### Example for ON-rep-seq

* In Downloads you can find examples (10 samples) of the the original data that can be analyzed, e.g. with the provided wrappers. 
* These samples are in fastq format (already basecalled) in a folder called "demultiplexed-data", therefore one can directly start with 2.peak_correction.sh 

### Contacts ###

* Repository is maintained by Łukasz Krych (e-mail: krych@food.ku.dk) and Josué L. Castro-Mejía (e-mail: jcame@food.ku.dk)
