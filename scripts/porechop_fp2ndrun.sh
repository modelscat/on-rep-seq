#!/bin/bash
INDIR="$1"
OUTDIR="$2"

if [ "$#" -ne 2 ]; then
    echo "Usage:  $0 <PORECHOP1stRUN_INDIR> <OUTDIR>"
    exit 1
fi

mkdir -p "$OUTDIR"

parallel "./porechop.py -i {} -o \"$OUTDIR\"/\$(basename {}) --fp2ndrun 2>&1 | tee \"$OUTDIR\"/\$(basename {}).porechop.fp2ndrun.log 1>&2" ::: "$INDIR/"*.fastq 

